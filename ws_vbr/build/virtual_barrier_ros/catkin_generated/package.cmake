set(_CATKIN_CURRENT_PACKAGE "virtual_barrier_ros")
set(virtual_barrier_ros_VERSION "0.1.0")
set(virtual_barrier_ros_MAINTAINER "Steven Hoang <khoahoang08091997@hotmail.com>")
set(virtual_barrier_ros_PACKAGE_FORMAT "1")
set(virtual_barrier_ros_BUILD_DEPENDS "pluginlib" "eigen" "moveit_core" "moveit_ros_planning" "moveit_ros_planning_interface" "geometric_shapes" "moveit_visual_tools" "tf2_ros" "tf2_eigen" "tf2_geometry_msgs" "message_generation" "actionlib_msgs" "std_msgs" "trajectory_msgs")
set(virtual_barrier_ros_BUILD_EXPORT_DEPENDS "panda_moveit_config" "pluginlib" "moveit_core" "moveit_commander" "moveit_fake_controller_manager" "moveit_ros_planning_interface" "moveit_visual_tools" "tf2_ros" "tf2_eigen" "tf2_geometry_msgs" "message_runtime" "std_msgs" "trajectory_msgs" "actionlib_msgs")
set(virtual_barrier_ros_BUILDTOOL_DEPENDS "catkin")
set(virtual_barrier_ros_BUILDTOOL_EXPORT_DEPENDS )
set(virtual_barrier_ros_EXEC_DEPENDS "panda_moveit_config" "pluginlib" "moveit_core" "moveit_commander" "moveit_fake_controller_manager" "moveit_ros_planning_interface" "moveit_visual_tools" "tf2_ros" "tf2_eigen" "tf2_geometry_msgs" "message_runtime" "std_msgs" "trajectory_msgs" "actionlib_msgs")
set(virtual_barrier_ros_RUN_DEPENDS "panda_moveit_config" "pluginlib" "moveit_core" "moveit_commander" "moveit_fake_controller_manager" "moveit_ros_planning_interface" "moveit_visual_tools" "tf2_ros" "tf2_eigen" "tf2_geometry_msgs" "message_runtime" "std_msgs" "trajectory_msgs" "actionlib_msgs")
set(virtual_barrier_ros_TEST_DEPENDS "moveit_resources")
set(virtual_barrier_ros_DOC_DEPENDS )
set(virtual_barrier_ros_URL_WEBSITE "")
set(virtual_barrier_ros_URL_BUGTRACKER "")
set(virtual_barrier_ros_URL_REPOSITORY "")
set(virtual_barrier_ros_DEPRECATED "")