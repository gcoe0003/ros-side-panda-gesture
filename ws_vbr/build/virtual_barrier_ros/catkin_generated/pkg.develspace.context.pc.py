# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/usr/include/eigen3".split(';') if "/usr/include/eigen3" != "" else []
PROJECT_CATKIN_DEPENDS = "moveit_core;moveit_ros_planning_interface;moveit_visual_tools;tf2_eigen;tf2_geometry_msgs;message_runtime;std_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "".split(';') if "" != "" else []
PROJECT_NAME = "virtual_barrier_ros"
PROJECT_SPACE_DIR = "/home/hrigroup2/ws_vbr/devel/.private/virtual_barrier_ros"
PROJECT_VERSION = "0.1.0"
