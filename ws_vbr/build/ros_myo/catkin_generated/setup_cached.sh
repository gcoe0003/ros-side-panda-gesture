#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/hrigroup2/ws_vbr/devel/.private/ros_myo:$CMAKE_PREFIX_PATH"
export PWD='/home/hrigroup2/ws_vbr/build/ros_myo'
export ROSLISP_PACKAGE_DIRECTORIES="/home/hrigroup2/ws_vbr/devel/.private/ros_myo/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/hrigroup2/ws_vbr/src/ros_myo:$ROS_PACKAGE_PATH"