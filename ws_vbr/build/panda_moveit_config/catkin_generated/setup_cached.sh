#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/hrigroup2/ws_vbr/devel/.private/panda_moveit_config:$CMAKE_PREFIX_PATH"
export PWD='/home/hrigroup2/ws_vbr/build/panda_moveit_config'
export ROSLISP_PACKAGE_DIRECTORIES="/home/hrigroup2/ws_vbr/devel/.private/panda_moveit_config/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/hrigroup2/ws_vbr/src/panda_moveit_config:$ROS_PACKAGE_PATH"