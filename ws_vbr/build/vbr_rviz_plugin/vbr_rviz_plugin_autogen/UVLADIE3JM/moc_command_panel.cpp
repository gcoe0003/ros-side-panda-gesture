/****************************************************************************
** Meta object code from reading C++ file 'command_panel.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../src/vbr_rviz_plugin/src/command_panel.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'command_panel.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_vbr_rviz_plugin__CommandPanel_t {
    QByteArrayData data[6];
    char stringdata0[69];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_vbr_rviz_plugin__CommandPanel_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_vbr_rviz_plugin__CommandPanel_t qt_meta_stringdata_vbr_rviz_plugin__CommandPanel = {
    {
QT_MOC_LITERAL(0, 0, 29), // "vbr_rviz_plugin::CommandPanel"
QT_MOC_LITERAL(1, 30, 8), // "lockPath"
QT_MOC_LITERAL(2, 39, 0), // ""
QT_MOC_LITERAL(3, 40, 7), // "execute"
QT_MOC_LITERAL(4, 48, 9), // "terminate"
QT_MOC_LITERAL(5, 58, 10) // "readyState"

    },
    "vbr_rviz_plugin::CommandPanel\0lockPath\0"
    "\0execute\0terminate\0readyState"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_vbr_rviz_plugin__CommandPanel[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   34,    2, 0x09 /* Protected */,
       3,    0,   35,    2, 0x09 /* Protected */,
       4,    0,   36,    2, 0x09 /* Protected */,
       5,    0,   37,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void vbr_rviz_plugin::CommandPanel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        CommandPanel *_t = static_cast<CommandPanel *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->lockPath(); break;
        case 1: _t->execute(); break;
        case 2: _t->terminate(); break;
        case 3: _t->readyState(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject vbr_rviz_plugin::CommandPanel::staticMetaObject = {
    { &rviz::Panel::staticMetaObject, qt_meta_stringdata_vbr_rviz_plugin__CommandPanel.data,
      qt_meta_data_vbr_rviz_plugin__CommandPanel,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *vbr_rviz_plugin::CommandPanel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *vbr_rviz_plugin::CommandPanel::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_vbr_rviz_plugin__CommandPanel.stringdata0))
        return static_cast<void*>(this);
    return rviz::Panel::qt_metacast(_clname);
}

int vbr_rviz_plugin::CommandPanel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = rviz::Panel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
