#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>

#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_msgs/ExecuteKnownTrajectory.h>

#include <moveit_visual_tools/moveit_visual_tools.h>

class Planner{
    public:
        Planner();
        void planningSceneManager(moveit_msgs::CollisionObject object);
        void descartesPathPlanner(geometry_msgs::PoseArray path);
    private:
        std::string PLANNING_GROUP = "panda_arm";
        const robot_state::JointModelGroup* joint_model_group;
        std::string BASE_FRAME = "panda_link0";
        moveit_msgs::PlanningScene planning_scene;

        // ROS Publisher/Subscriber system
        ros::NodeHandle node_handle;
        ros::Subscriber collision_object_manager;
        ros::Subscriber path_planner;
        ros::Publisher planning_scene_diff_publisher;
        ros::Publisher display_path;
        ros::Publisher debug;
};
// Constructor
Planner::Planner(){
    collision_object_manager = node_handle.subscribe("collision_object_manager", 1, &Planner::planningSceneManager, this);
    path_planner = node_handle.subscribe("planning_path", 1, &Planner::descartesPathPlanner, this);
    planning_scene_diff_publisher = node_handle.advertise<moveit_msgs::PlanningScene>("planning_scene", 1);
    display_path = node_handle.advertise<trajectory_msgs::JointTrajectory>("display_path", 1);
    debug = node_handle.advertise<moveit_msgs::RobotTrajectory>("debug", 1000);
}
// Adding/Removing CollisionObjects onto the scene
void Planner::planningSceneManager(moveit_msgs::CollisionObject object){
    std::string object_id = object.id;
    ROS_INFO("Object on operation: %s", object_id.c_str());
    planning_scene.world.collision_objects.push_back(object);
    planning_scene.is_diff = true;
    planning_scene_diff_publisher.publish(planning_scene);
}
// Testing with MoveGroupInterface, gonna change to DescartesPlanner later
void Planner::descartesPathPlanner(geometry_msgs::PoseArray path){
    moveit::planning_interface::MoveGroupInterface move_group(PLANNING_GROUP);
    joint_model_group = 
        move_group.getCurrentState()->getJointModelGroup(PLANNING_GROUP);
    // Visual tools
    moveit_visual_tools::MoveItVisualTools visual_tools(BASE_FRAME);
    visual_tools.deleteAllMarkers();
    bool success;
    for (int i = 0; i < path.poses.size(); i++){
        move_group.setPoseTarget(path.poses[i]);
        moveit::planning_interface::MoveGroupInterface::Plan my_plan;
        success = (move_group.plan(my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
        debug.publish(my_plan.trajectory_);
        ROS_INFO("Visualising (pose goal) %s", success ? "SUCCESS" : "FAILED");
        visual_tools.publishTrajectoryLine(my_plan.trajectory_, joint_model_group);
        visual_tools.trigger();
        ROS_INFO("Visualising trajectory line");
        ROS_INFO("Moving to the target pose");
        success = (move_group.execute(my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
        ROS_INFO("Moved to target pose: %s", success ? "SUCCESS" : "FAILED");
        move_group.clearPoseTarget();
        display_path.publish(my_plan.trajectory_.joint_trajectory);
    }
}

int main(int argc, char** argv){
    ros::init(argc, argv, "descartes_planner");
    ros::NodeHandle node_handle;
    ros::AsyncSpinner spinner(4);
    spinner.start();
    Planner myPlanner;
    ros::waitForShutdown();
    return 0;
}
