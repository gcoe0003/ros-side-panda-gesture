// This would be implemented as a function in Unity to communicate with ROS
#include <ros/ros.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>
#include <std_msgs/Bool.h>
#include <std_msgs/UInt32.h>
#include <geometry_msgs/Polygon.h>
#include <geometry_msgs/Point32.h>
#include <geometry_msgs/PoseArray.h>

int main(int argc, char **argv)
{
    ros::init(argc, argv, "collision_manager");
    ros::NodeHandle node_handle;

    ros::AsyncSpinner spinner(1);
    spinner.start();
    // Set up a publisher which publishs Collision Object msg onto object_adder topic
    ros::Publisher unity_publisher_test = node_handle.advertise<moveit_msgs::PlanningSceneWorld>("collision_object_manager", 1);
    // This publisher would signal the planner in ROS to plan the desired path
    ros::Publisher planning_path = node_handle.advertise<geometry_msgs::PoseArray>("planning_path", 1000);
    ros::Publisher robot_command = node_handle.advertise<std_msgs::UInt32>("vb_robot_command", 1);
    ros::Rate loop_rate(1);

    // Setup
    static const std::string PLANNING_GROUP = "panda_arm"; // Used to be "manipulator"
    moveit::planning_interface::MoveGroupInterface movegroup(PLANNING_GROUP);
    // Create a message to publish
    moveit_msgs::CollisionObject collision_box;
    collision_box.header.frame_id = movegroup.getPlanningFrame();

    collision_box.id = "box1";

    shape_msgs::SolidPrimitive box;
    box.type = box.BOX;
    box.dimensions.resize(3);
    box.dimensions[0] = 0.1;
    box.dimensions[1] = 0.1;
    box.dimensions[2] = 0.1;

    geometry_msgs::Pose box_pose;
    box_pose.orientation.w = 1.0;
    box_pose.position.x = 0.4;
    box_pose.position.y = -0.2;
    box_pose.position.z = 0.2;

    collision_box.primitives.push_back(box);
    collision_box.primitive_poses.push_back(box_pose);
    collision_box.operation = collision_box.ADD;

    moveit_msgs::CollisionObject collision_box_copied;
    collision_box_copied.header.frame_id = movegroup.getPlanningFrame();

    collision_box_copied.id = "box2";

    shape_msgs::SolidPrimitive box_copied;
    box_copied.type = box_copied.BOX;
    box_copied.dimensions.resize(3);
    box_copied.dimensions[0] = 0.1;
    box_copied.dimensions[1] = 0.1;
    box_copied.dimensions[2] = 0.1;

    geometry_msgs::Pose box_pose_copied;
    box_pose_copied.orientation.w = 1.0;
    box_pose_copied.position.x = 0.4;
    box_pose_copied.position.y = 0.45;
    box_pose_copied.position.z = 0.12;

    collision_box_copied.primitives.push_back(box_copied);
    collision_box_copied.primitive_poses.push_back(box_pose_copied);
    collision_box_copied.operation = collision_box_copied.ADD;

    // Workspace
    moveit_msgs::CollisionObject workspace;
    workspace.header.frame_id = movegroup.getPlanningFrame();

    workspace.id = "workspace";

    shape_msgs::SolidPrimitive workspace_sp;
    workspace_sp.type = box_copied.BOX;
    workspace_sp.dimensions.resize(3);
    workspace_sp.dimensions[0] = 1;
    workspace_sp.dimensions[1] = 1;
    workspace_sp.dimensions[2] = 0.01;

    geometry_msgs::Pose workspace_pose;
    workspace_pose.orientation.w = 1.0;
    workspace_pose.position.x = 0;
    workspace_pose.position.y = 0;
    workspace_pose.position.z = 0;

    workspace.primitives.push_back(workspace_sp);
    workspace.primitive_poses.push_back(workspace_pose);
    workspace.operation = workspace.ADD;

    // PlanningSceneWorld msg
    moveit_msgs::PlanningSceneWorld world;
    world.collision_objects.push_back(workspace);

    // Planning path
    std::vector<geometry_msgs::Pose> cart_path;
    geometry_msgs::Pose my_pose, original_pose;
    // Create an original pose
    original_pose = movegroup.getCurrentPose().pose;
    my_pose = movegroup.getCurrentPose().pose;

    // ROS_INFO("%f, %f, %f", original_pose.position.x, original_pose.position.y, original_pose.position.z);
    // Start in the middle
    // cart_path.push_back(original_pose);

    // Move to the left of the left box
    my_pose.orientation = original_pose.orientation;
    my_pose.position.x = 0.4;
    my_pose.position.y = -0.45;
    my_pose.position.z = 0.12;
    cart_path.push_back(my_pose);

    //Move into to the centre
    my_pose.position.y = 0;
    cart_path.push_back(my_pose);

    //Move to the right of the right box
    my_pose.position.y = 0.45;
    cart_path.push_back(my_pose);

    //Move to the centre again
    cart_path.push_back(original_pose);

    // Move to the left of the left box
    my_pose.orientation = original_pose.orientation;
    my_pose.position.x = 0.4;
    my_pose.position.y = -0.45;
    my_pose.position.z = 0.22;
    cart_path.push_back(my_pose);

    //Move into to the centre
    my_pose.position.y = 0;
    cart_path.push_back(my_pose);

    //Move to the right of the right box
    my_pose.position.y = 0.45;
    cart_path.push_back(my_pose);

    //Move to the centre again
    cart_path.push_back(original_pose);

    // Move to the left of the left box
    my_pose.orientation = original_pose.orientation;
    my_pose.position.x = 0.4;
    my_pose.position.y = -0.45;
    my_pose.position.z = 0.32;
    cart_path.push_back(my_pose);

    //Move into to the centre
    my_pose.position.y = 0;
    cart_path.push_back(my_pose);

    //Move to the right of the right box
    my_pose.position.y = 0.45;
    cart_path.push_back(my_pose);

    //Move to the centre again
    cart_path.push_back(original_pose);

    geometry_msgs::PoseArray cart_array;
    cart_array.poses = cart_path;
    // // Debugging purpose: check the size of PoseArray.poses
    // ROS_INFO("PoseArray size: %lu", cart_array.poses.size());

    // Publish message onto the topics
    int counter = 0;
    while (ros::ok)
    {
        if (counter == 1)
        {
            unity_publisher_test.publish(world);
            ROS_INFO("Test");
        }
        else if (counter == 3)
        {
            // world.collision_objects.clear();
            // world.collision_objects.push_back(workspace);
            // world.collision_objects.push_back(collision_box);
            // unity_publisher_test.publish(world);
            // ROS_INFO("Left box");
        }
        else if (counter == 13)
        {
            // world.collision_objects.clear();
            // world.collision_objects.push_back(workspace);
            // world.collision_objects.push_back(collision_box);
            // world.collision_objects.push_back(collision_box_copied);
            // unity_publisher_test.publish(world);
            // ROS_INFO("Right box");
        }
        else if (counter == 5)
        {
            planning_path.publish(cart_array);
            ROS_INFO("Cartesian Path published");
        }
        else if (counter == 8)
        {
            std_msgs::UInt32 command;
            command.data = 1;
            robot_command.publish(command);
            ROS_INFO("Execute flag");
        }
        else if (counter == 35)
        {
            // box_pose_copied.position.x = 0.4;
            // box_pose_copied.position.y = 0.2;
            // box_pose_copied.position.z = 0.2;
            // collision_box_copied.primitive_poses.clear();
            // collision_box_copied.primitive_poses.push_back(box_pose_copied);
            // world.collision_objects.clear();
            // world.collision_objects.push_back(workspace);
            // world.collision_objects.push_back(collision_box);
            // world.collision_objects.push_back(collision_box_copied);
            // unity_publisher_test.publish(world);
            // ROS_INFO("Right box moved");
            // std_msgs::Bool flag;
            // flag.data = true;
            // ready_state.publish(flag);
            // ROS_INFO("Reset ready state flag");
        }
        counter++;
        loop_rate.sleep();
    }
    return 0;
}
