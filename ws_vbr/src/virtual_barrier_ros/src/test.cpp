#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>

#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <std_msgs/Bool.h>
#include <std_msgs/UInt32.h>
#include <geometry_msgs/Polygon.h>
#include <geometry_msgs/Point32.h>
#include <geometry_msgs/PoseArray.h>

int main(int argc, char **argv)
{
  ros::init(argc, argv, "motion_test");
  ros::NodeHandle node_handle;
  ros::AsyncSpinner spinner(1);
  spinner.start();

  //Setup
  static const std::string PLANNING_GROUP = "panda_arm";
  moveit::planning_interface::MoveGroupInterface move_group(PLANNING_GROUP);
  moveit::planning_interface::PlanningSceneInterface planning_scene_interface;
  const robot_state::JointModelGroup *joint_model_group =
      move_group.getCurrentState()->getJointModelGroup(PLANNING_GROUP);

  moveit::planning_interface::MoveGroupInterface::Plan my_plan;
  moveit::core::RobotStatePtr current_state = move_group.getCurrentState();

  std::vector<double> joint_group_position;

  current_state->copyJointGroupPositions(joint_model_group, joint_group_position);
  // Planning to a Pose goal
  // ^^^^^^^^^^^^^^^^^^^^^^^
  // We can plan a motion for this group to a desired pose for the
  // end-effector.
  geometry_msgs::Pose target_pose1;
  target_pose1.orientation = move_group.getCurrentPose().pose.orientation;
  target_pose1.position.x = 0.4;
  target_pose1.position.y = 0.2;
  target_pose1.position.z = 0.2;
  move_group.setPoseTarget(target_pose1);

  move_group.setMaxAccelerationScalingFactor(0.02);
  move_group.setMaxVelocityScalingFactor(0.02);
  // Now, we call the planner to compute the plan and visualize it.
  // Note that we are just planning, not asking move_group
  // to actually move the robot.
  bool success = (move_group.plan(my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);

  move_group.asyncExecute(my_plan);
  ROS_INFO("Sent move request. Sleep for 10 sec");
  ros::Duration(10.0).sleep();
  ROS_INFO("Woke up! Terminate the robot");
  move_group.stop();
  ROS_INFO("Sleep for 5 secs!");
  ros::Duration(5.0).sleep();
  ROS_INFO("Move back to ready state");

  move_group.setJointValueTarget(joint_group_position);
  success = (move_group.plan(my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
  move_group.asyncExecute(my_plan);
  ROS_INFO("Sent request back to ready state");

  ros::shutdown();
}
