#include <pluginlib/class_loader.h>
#include <ros/ros.h>

// MoveIt
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_state/conversions.h>
#include <moveit/planning_pipeline/planning_pipeline.h>
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>

#include <moveit/collision_detection/collision_matrix.h>

#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/kinematic_constraints/utils.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_msgs/PlanningScene.h>
#include <moveit_msgs/CollisionObject.h>
#include <moveit_msgs/ExecuteTrajectoryAction.h>
#include <std_msgs/Bool.h>
#include <std_msgs/UInt32.h>
#include <std_msgs/Float32MultiArray.h>
#include <geometry_msgs/Polygon.h>
#include <geometry_msgs/Point32.h>
#include <geometry_msgs/PoseArray.h>

// TF2
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

class Planner
{
public:
    Planner(ros::NodeHandle* node_handle_ptr, ros::NodeHandle* node_handle_private_ptr);
    void planningSceneManager(moveit_msgs::PlanningSceneWorld world);
    void picker(moveit_msgs::CollisionObject pickObj);
    void placer(geometry_msgs::Vector3 placePos);
    void pathPlanner(geometry_msgs::PoseArray path);
    void trajectoryPoints(moveit_msgs::DisplayTrajectory robot_trajectory);
    void robotCommand(std_msgs::UInt32 command);
    void executionProcess(moveit_msgs::ExecuteTrajectoryActionResult result);
    void replan(const ros::TimerEvent& event);
    void unityStateUpdate(const ros::TimerEvent& event);
    bool checkValidity();
    void pick(moveit::planning_interface::MoveGroupInterface& move_group);
    void place(moveit::planning_interface::MoveGroupInterface& group, double placePos[]);
    void openGripper(trajectory_msgs::JointTrajectory& posture);
    void closedGripper(trajectory_msgs::JointTrajectory& posture);
    void addCollisionObjects(moveit::planning_interface::PlanningSceneInterface& planning_scene_interface, double objectAxis);
    void toggleCollisionGripper(std::string objectStr, bool toggleWorkspace, bool toggleObject);


    // Helper function
    std::vector<int> vectorsort(std::vector<int> vector);

private:
    const std::string PLANNING_GROUP = "panda_arm"; // Used to be "panda_arm"
    const std::string BASE_FRAME = "panda_link0";   //Used to be "panda_link0"
    const std::string EEF = "panda_link8";
    // Robot
    robot_model_loader::RobotModelLoaderPtr robot_model_loader;
    robot_model::RobotModelPtr robot_model;
    robot_state::RobotStatePtr robot_state_holder;
    robot_state::RobotStatePtr forward_kinematic_solver;
    std::vector<moveit_msgs::RobotState> traj_start_state_msg; // Start state robot_state_msgs for reference
    std::vector<robot_state::RobotState> waypoint_robot_state; // Robot state at each waypoint, including the starting and ending waypoints

    const robot_state::JointModelGroup* joint_model_group;
    std::vector<std::vector<std::size_t>> index;
    std::vector<int> classification;

    // Planning scene monitor
    planning_scene_monitor::PlanningSceneMonitorPtr psm;
    bool planning = false;
    int plan_delay = 0;
    int replan_attempts_count = 0;
    const int REPLAN_FAIL_MAX = 3;
    bool replan_flag = false;
    std::vector<moveit_msgs::CollisionObject> collision_objects;

    // Planner
    planning_pipeline::PlanningPipelinePtr planning_pipeline;
    planning_interface::MotionPlanRequest req;
    planning_interface::MotionPlanResponse res;

    std::vector<double> tolerance_pose;
    std::vector<double> tolerance_angle;

    // Saved goalpoints array
    geometry_msgs::PoseArray input_path;
    // Divided into stages
    std::vector<geometry_msgs::PoseArray> stages;
    // The set of goalpoints that the robot is executing
    geometry_msgs::PoseArray executing_path;
    // Trajectory(es) for executing
    moveit_msgs::DisplayTrajectory robot_trajectory;

    moveit_msgs::CollisionObject workspace;
    moveit_msgs::CollisionObject table;
    moveit_msgs::CollisionObject penPlaceholder;
    moveit_msgs::CollisionObject eraserPlaceholder;
    moveit_msgs::CollisionObject adjustLower;
    moveit_msgs::CollisionObject toggleLower;
    moveit_msgs::CollisionObject adjustHigher;
    moveit_msgs::CollisionObject obstacle1;
    moveit_msgs::CollisionObject obstacle2;
    moveit_msgs::CollisionObject obstacle3;

    // ROS Subscriber/Publisher system

    ros::NodeHandle node_handle_;
    ros::NodeHandle node_handle_private_;

    ros::Subscriber collision_object_manager;
    ros::Subscriber pick_command;
    ros::Subscriber place_command;
    ros::Subscriber path_planner;
    ros::Subscriber vb_robot_command;

    ros::Publisher planning_scene_diff_publisher;
    ros::Publisher trajectory_preview;
    ros::Publisher unityStatePublisher;
    ros::Publisher trajectory_waypoints;

    ros::Timer replan_callback;
    float replan_frequency = 1.0f;
    ros::Timer state_callback;
    float state_publish_frequency = 0.5f;

    // ROS Execution, using MoveGroupInterface, with execute() and stop() function
    moveit::planning_interface::MoveGroupInterfacePtr movegroup_ptr;
    ros::Subscriber execution_status;
    bool execution_flag = false;
    bool executing = false;
    bool temp_stop = false;
    bool ready_state_flag = false;
    bool pickPlaceFlag = true;
    bool pauseScene = false;
    bool gripperOpen = false;

    tf2::Quaternion objectOrientation;
    std::string objectName;
    double objectPos[3];
    double objectHeight;
};
// Constructor
Planner::Planner(ros::NodeHandle* node_handle_ptr, ros::NodeHandle* node_handle_private_ptr) : node_handle_(*node_handle_ptr), node_handle_private_(*node_handle_private_ptr)
{
    // Advertise and subscribe to topics
    planning_scene_diff_publisher = node_handle_.advertise<moveit_msgs::PlanningScene>("planning_scene", 1);
    trajectory_preview = node_handle_.advertise<moveit_msgs::DisplayTrajectory>("display_trajectory", 1, true);
    trajectory_waypoints = node_handle_.advertise<geometry_msgs::PoseArray>("trajectory_waypoints", 1, true);
    unityStatePublisher = node_handle_.advertise<std_msgs::UInt32>("system_state", 1, true);

    replan_callback = node_handle_.createTimer(ros::Duration(replan_frequency), &Planner::replan, this);
    state_callback = node_handle_.createTimer(ros::Duration(state_publish_frequency), &Planner::unityStateUpdate, this);

    collision_object_manager = node_handle_.subscribe("collision_object_manager", 1000, &Planner::planningSceneManager, this);
    pick_command = node_handle_.subscribe("picker", 1, &Planner::picker, this);
    place_command = node_handle_.subscribe("placer", 1, &Planner::placer, this);
    path_planner = node_handle_.subscribe("planning_path", 1, &Planner::pathPlanner, this);
    vb_robot_command = node_handle_.subscribe("/vb_robot_command", 1, &Planner::robotCommand, this);
    execution_status = node_handle_.subscribe("/execute_trajectory/result", 1, &Planner::executionProcess, this);

    // Load robot model and planning scene with the robot model in it

    robot_model_loader.reset(new robot_model_loader::RobotModelLoader("robot_description"));
    psm.reset(new planning_scene_monitor::PlanningSceneMonitor(robot_model_loader));

    psm->startSceneMonitor();
    psm->startWorldGeometryMonitor();
    psm->startStateMonitor();

    robot_model = robot_model_loader->getModel();
    robot_state_holder.reset(new robot_state::RobotState(planning_scene_monitor::LockedPlanningSceneRO(psm)->getCurrentState()));
    joint_model_group = robot_state_holder->getJointModelGroup(PLANNING_GROUP);

    // Using RobotState as a forward kinematic solver
    forward_kinematic_solver.reset(new robot_state::RobotState(planning_scene_monitor::LockedPlanningSceneRO(psm)->getCurrentState()));
    // Load planning pipeline
    planning_pipeline.reset(new planning_pipeline::PlanningPipeline(robot_model, node_handle_private_, "planning_plugin", "request_adapters"));

    // Setup Planning group for the robot
    req.group_name = PLANNING_GROUP;

    // Setup Planner Tolarence
    for (int i = 0; i < 3; i++)
    {
        tolerance_angle.push_back(0.005);
        tolerance_pose.push_back(0.002);
    }

    req.max_velocity_scaling_factor = 0.2f;
    req.max_acceleration_scaling_factor = 0.1f;

    // Setup timeout for planner
    req.allowed_planning_time = 2.0f;

    // Setup robot controller using MoveGroup
    movegroup_ptr.reset(new moveit::planning_interface::MoveGroupInterface(PLANNING_GROUP));

    // penPlaceholder
    penPlaceholder.id = "Pen Placeholder";
    penPlaceholder.header.frame_id = "world";
    shape_msgs::SolidPrimitive penPlaceholder_sp;
    penPlaceholder_sp.type = penPlaceholder_sp.BOX;
    penPlaceholder_sp.dimensions.resize(3);
    penPlaceholder_sp.dimensions[0] = 0.05;
    penPlaceholder_sp.dimensions[1] = 0.05;
    penPlaceholder_sp.dimensions[2] = 0.16;

    geometry_msgs::Pose penPlaceholder_pose;
    penPlaceholder_pose.orientation.w = 1.0;
    moveit::planning_interface::MoveGroupInterface group("panda_arm");
    geometry_msgs::Pose target_pose = group.getCurrentPose().pose;

    penPlaceholder_pose.position.x = target_pose.position.x;
    penPlaceholder_pose.position.y = target_pose.position.y;
    penPlaceholder_pose.position.z = target_pose.position.z - 0.13;
    penPlaceholder.primitives.push_back(penPlaceholder_sp);
    penPlaceholder.primitive_poses.push_back(penPlaceholder_pose);
    penPlaceholder.operation = penPlaceholder.ADD;

    planning_scene_monitor::LockedPlanningSceneRW(psm)->processCollisionObjectMsg(penPlaceholder);

    // eraserPlaceholder
    eraserPlaceholder.id = "Eraser Placeholder";
    eraserPlaceholder.header.frame_id = "world";
    shape_msgs::SolidPrimitive eraserPlaceholder_sp;
    eraserPlaceholder_sp.type = eraserPlaceholder_sp.BOX;
    eraserPlaceholder_sp.dimensions.resize(3);
    eraserPlaceholder_sp.dimensions[0] = 0.16;
    eraserPlaceholder_sp.dimensions[1] = 0.044;
    eraserPlaceholder_sp.dimensions[2] = 0.08;

    geometry_msgs::Pose eraserPlaceholder_pose;
    eraserPlaceholder_pose.orientation.w = 1.0;

    eraserPlaceholder_pose.position.x = target_pose.position.x;
    eraserPlaceholder_pose.position.y = target_pose.position.y;
    eraserPlaceholder_pose.position.z = target_pose.position.z - 0.11;
    eraserPlaceholder.primitives.push_back(eraserPlaceholder_sp);
    eraserPlaceholder.primitive_poses.push_back(eraserPlaceholder_pose);
    eraserPlaceholder.operation = eraserPlaceholder.ADD;

    planning_scene_monitor::LockedPlanningSceneRW(psm)->processCollisionObjectMsg(eraserPlaceholder);

    // adjustLower
    adjustLower.id = "5mm lower";
    adjustLower.header.frame_id = "world";
    shape_msgs::SolidPrimitive adjustLower_sp;
    adjustLower_sp.type = adjustLower_sp.BOX;
    adjustLower_sp.dimensions.resize(3);
    adjustLower_sp.dimensions[0] = 0.05;
    adjustLower_sp.dimensions[1] = 0.05;
    adjustLower_sp.dimensions[2] = 0.15;

    geometry_msgs::Pose adjustLower_pose;
    adjustLower_pose.orientation.w = 1.0;

    adjustLower_pose.position.x = 0;
    adjustLower_pose.position.y = 1;
    adjustLower_pose.position.z = -1;
    adjustLower.primitives.push_back(adjustLower_sp);
    adjustLower.primitive_poses.push_back(adjustLower_pose);
    adjustLower.operation = adjustLower.ADD;

    planning_scene_monitor::LockedPlanningSceneRW(psm)->processCollisionObjectMsg(adjustLower);

    // adjustHigher
    adjustHigher.id = "5mm higher";
    adjustHigher.header.frame_id = "world";
    shape_msgs::SolidPrimitive adjustHigher_sp;
    adjustHigher_sp.type = adjustHigher_sp.BOX;
    adjustHigher_sp.dimensions.resize(3);
    adjustHigher_sp.dimensions[0] = 0.05;
    adjustHigher_sp.dimensions[1] = 0.05;
    adjustHigher_sp.dimensions[2] = 0.15;

    geometry_msgs::Pose adjustHigher_pose;
    adjustHigher_pose.orientation.w = 1.0;

    adjustHigher_pose.position.x = 0;
    adjustHigher_pose.position.y = -1;
    adjustHigher_pose.position.z = -1;
    adjustHigher.primitives.push_back(adjustHigher_sp);
    adjustHigher.primitive_poses.push_back(adjustHigher_pose);
    adjustHigher.operation = adjustHigher.ADD;

    planning_scene_monitor::LockedPlanningSceneRW(psm)->processCollisionObjectMsg(adjustHigher);

    // toggleLower
    toggleLower.id = "Toggle Lower";
    toggleLower.header.frame_id = "world";
    shape_msgs::SolidPrimitive toggleLower_sp;
    toggleLower_sp.type = toggleLower_sp.BOX;
    toggleLower_sp.dimensions.resize(3);
    toggleLower_sp.dimensions[0] = 0.05;
    toggleLower_sp.dimensions[1] = 0.05;
    toggleLower_sp.dimensions[2] = 0.15;

    geometry_msgs::Pose toggleLower_pose;
    toggleLower_pose.orientation.w = 1.0;

    toggleLower_pose.position.x = 0;
    toggleLower_pose.position.y = 1;
    toggleLower_pose.position.z = -1;
    toggleLower.primitives.push_back(toggleLower_sp);
    toggleLower.primitive_poses.push_back(toggleLower_pose);
    toggleLower.operation = toggleLower.ADD;

    planning_scene_monitor::LockedPlanningSceneRW(psm)->processCollisionObjectMsg(toggleLower);
/*
        // obstacle1
    obstacle1.id = "Obstacle1";
    obstacle1.header.frame_id = "world";
    shape_msgs::SolidPrimitive obstacle1_sp;
    obstacle1_sp.type = obstacle1_sp.BOX;
    obstacle1_sp.dimensions.resize(3);
    obstacle1_sp.dimensions[0] = 0.22;
    obstacle1_sp.dimensions[1] = 0.12;
    obstacle1_sp.dimensions[2] = 0.24;

    // Second number is co-ordinate for right back bottom corner
    geometry_msgs::Pose obstacle1_pose;
    obstacle1_pose.orientation.w = 1.0;
    obstacle1_pose.position.x = -0.11 - 0.5;
    obstacle1_pose.position.y = -0.06 - 0.2;
    obstacle1_pose.position.z = 0.12;
    obstacle1.primitives.push_back(obstacle1_sp);
    obstacle1.primitive_poses.push_back(obstacle1_pose);
    obstacle1.operation = obstacle1.ADD;

    planning_scene_monitor::LockedPlanningSceneRW(psm)->processCollisionObjectMsg(obstacle1);

    // obstacle2
    obstacle2.id = "Obstacle2";
    obstacle2.header.frame_id = "world";
    shape_msgs::SolidPrimitive obstacle2_sp;
    obstacle2_sp.type = obstacle2_sp.BOX;
    obstacle2_sp.dimensions.resize(3);
    obstacle2_sp.dimensions[0] = 0.26;
    obstacle2_sp.dimensions[1] = 0.14;
    obstacle2_sp.dimensions[2] = 0.28;

    // Second number is co-ordinate for right front bottom corner
    geometry_msgs::Pose obstacle2_pose;
    obstacle2_pose.orientation.w = 1.0;
    obstacle2_pose.position.x = -0.11 - 0.5;
    obstacle2_pose.position.y = 0.07 + 0.2;
    obstacle2_pose.position.z = 0.12;
    obstacle2.primitives.push_back(obstacle2_sp);
    obstacle2.primitive_poses.push_back(obstacle2_pose);
    obstacle2.operation = obstacle2.ADD;

    planning_scene_monitor::LockedPlanningSceneRW(psm)->processCollisionObjectMsg(obstacle2);

    // obstacle3
    obstacle3.id = "Obstacle3";
    obstacle3.header.frame_id = "world";
    shape_msgs::SolidPrimitive obstacle3_sp;
    obstacle3_sp.type = obstacle3_sp.BOX;
    obstacle3_sp.dimensions.resize(3);
    obstacle3_sp.dimensions[0] = 0.09;
    obstacle3_sp.dimensions[1] = 0.12;
    obstacle3_sp.dimensions[2] = 0.10;

    // Second number is co-ordinate for left back bottom corner
    geometry_msgs::Pose obstacle3_pose;
    obstacle3_pose.orientation.w = 1.0;
    obstacle3_pose.position.x = 0.045 + 0.51;
    obstacle3_pose.position.y = -0.22 + 0.06;
    obstacle3_pose.position.z = 0.05 + 0.07;
    obstacle3.primitives.push_back(obstacle3_sp);
    obstacle3.primitive_poses.push_back(obstacle3_pose);
    obstacle3.operation = obstacle3.ADD;

    planning_scene_monitor::LockedPlanningSceneRW(psm)->processCollisionObjectMsg(obstacle3);
*/
    // Update the scene in Rviz
    moveit_msgs::PlanningScene scene;
    planning_scene_monitor::LockedPlanningSceneRO(psm)->getPlanningSceneMsg(scene);
    planning_scene_diff_publisher.publish(scene);

    collision_detection::AllowedCollisionMatrix acm_no_collisions(scene.allowed_collision_matrix);

    acm_no_collisions.setEntry("panda_link8", "Pen Placeholder", true);
    acm_no_collisions.setEntry("panda_link7", "Pen Placeholder", true);
    acm_no_collisions.setEntry("panda_hand", "Pen Placeholder", true);
    acm_no_collisions.setEntry("panda_link8", "Eraser Placeholder", true);
    acm_no_collisions.setEntry("panda_link7", "Eraser Placeholder", true);
    acm_no_collisions.setEntry("panda_hand", "Eraser Placeholder", true);
    acm_no_collisions.setEntry("Eraser Placeholder", "Pen Placeholder", true);

    moveit_msgs::PlanningScene ps_no_collisions = scene;
    acm_no_collisions.getMessage(ps_no_collisions.allowed_collision_matrix);
    moveit::planning_interface::PlanningSceneInterface planning_scene_interface;
    planning_scene_interface.applyPlanningScene(ps_no_collisions);
    bool success1 = psm->requestPlanningSceneState("/get_planning_scene");

    group.attachObject("Pen Placeholder");
    group.attachObject("Eraser Placeholder");
    group.attachObject("5mm lower", "panda_link0");
    group.attachObject("5mm higher", "panda_link0");
    group.attachObject("Toggle Lower", "panda_link0");
    group.attachObject("Obstacle1", "panda_link0");
    group.attachObject("Obstacle2", "panda_link0");
    group.attachObject("Obstacle3", "panda_link0");

}
// Adding/Removing CollisionObjects onto the scene, also check for replan if neccessary
void Planner::planningSceneManager(moveit_msgs::PlanningSceneWorld world)
{
    if (!planning && !executing && !pauseScene) // Scene should be locked (not updated) when planning is carried out
    {
        collision_objects.resize((int)world.collision_objects.size());
        collision_objects = world.collision_objects;
        planning_scene_monitor::LockedPlanningSceneRW(psm)->removeAllCollisionObjects();

        bool success = planning_scene_monitor::LockedPlanningSceneRW(psm)->processPlanningSceneWorldMsg(world);

        workspace.id = "Whiteboard";
        workspace.header.frame_id = "world";
        shape_msgs::SolidPrimitive workspace_sp;
        workspace_sp.type = workspace_sp.BOX;
        workspace_sp.dimensions.resize(3);
        workspace_sp.dimensions[0] = 0.56;
        workspace_sp.dimensions[1] = 0.86;
        workspace_sp.dimensions[2] = 0.02;

        geometry_msgs::Pose workspace_pose;
        workspace_pose.orientation.w = 1.0;
        workspace_pose.position.x = 0.5;
        workspace_pose.position.y = -0.04;
        workspace_pose.position.z = 0.03;
        workspace.primitives.push_back(workspace_sp);
        workspace.primitive_poses.push_back(workspace_pose);
        workspace.operation = workspace.ADD;

        planning_scene_monitor::LockedPlanningSceneRW(psm)->processCollisionObjectMsg(workspace);

        table.id = "Workspace1";
        table.header.frame_id = "world";
        shape_msgs::SolidPrimitive table_sp;
        table_sp.type = table_sp.BOX;
        table_sp.dimensions.resize(3);
        table_sp.dimensions[0] = 1.8;
        table_sp.dimensions[1] = 1.2;
        table_sp.dimensions[2] = 0.02;

        geometry_msgs::Pose table_pose;
        table_pose.orientation.w = 1.0;
        table_pose.position.x = 0.0;
        table_pose.position.y = 0.1;
        table_pose.position.z = -0.03;
        table.primitives.push_back(table_sp);
        table.primitive_poses.push_back(table_pose);
        table.operation = table.ADD;

        planning_scene_monitor::LockedPlanningSceneRW(psm)->processCollisionObjectMsg(table);

        // Update the scene in Rviz
        moveit_msgs::PlanningScene scene;
        planning_scene_monitor::LockedPlanningSceneRO(psm)->getPlanningSceneMsg(scene);
        planning_scene_diff_publisher.publish(scene);

        collision_detection::AllowedCollisionMatrix acm_no_collisions(scene.allowed_collision_matrix);

        acm_no_collisions.setEntry("panda_link1", "Workspace1", true);
        acm_no_collisions.setEntry("panda_link0", "Workspace1", true);

        moveit::planning_interface::PlanningSceneInterface planning_scene_interface;

        if (planning_scene_interface.getAttachedObjects().count("Pen Placeholder") != 0)
        {
            acm_no_collisions.setEntry("panda_link8", "Pen Placeholder", true);
            acm_no_collisions.setEntry("panda_link7", "Pen Placeholder", true);
            acm_no_collisions.setEntry("panda_hand", "Pen Placeholder", true);
            acm_no_collisions.setEntry("panda_leftfinger", "Pen Placeholder", true);
            acm_no_collisions.setEntry("panda_rightfinger", "Pen Placeholder", true);
        }

        moveit_msgs::PlanningScene ps_no_collisions = scene;
        acm_no_collisions.getMessage(ps_no_collisions.allowed_collision_matrix);

        planning_scene_interface.applyPlanningScene(ps_no_collisions);
        bool success1 = psm->requestPlanningSceneState("/get_planning_scene");

    }
}
void Planner::toggleCollisionGripper(std::string objectStr, bool toggleWorkspace, bool toggleObject)
{
    moveit_msgs::PlanningScene scene;
    planning_scene_monitor::LockedPlanningSceneRO(psm)->getPlanningSceneMsg(scene);
    planning_scene_diff_publisher.publish(scene);

    collision_detection::AllowedCollisionMatrix acm_no_collisions(scene.allowed_collision_matrix);

    acm_no_collisions.setEntry("panda_link8", objectStr, toggleObject);
    acm_no_collisions.setEntry("panda_link7", objectStr, toggleObject);
    acm_no_collisions.setEntry("panda_hand", objectStr, toggleObject);
    acm_no_collisions.setEntry("Workspace1", objectStr, toggleWorkspace);
    acm_no_collisions.setEntry("panda_link8", "Workspace1", toggleWorkspace);
    acm_no_collisions.setEntry("panda_link7", "Workspace1", toggleWorkspace);
    acm_no_collisions.setEntry("panda_hand", "Workspace1", toggleWorkspace);
    acm_no_collisions.setEntry("panda_leftfinger", "Workspace1", toggleWorkspace);
    acm_no_collisions.setEntry("panda_rightfinger", "Workspace1", toggleWorkspace);

    moveit_msgs::PlanningScene ps_no_collisions = scene;
    acm_no_collisions.getMessage(ps_no_collisions.allowed_collision_matrix);

    moveit::planning_interface::PlanningSceneInterface planning_scene_interface;
    planning_scene_interface.applyPlanningScene(ps_no_collisions);
    bool success1 = psm->requestPlanningSceneState("/get_planning_scene");
}
// PickCommand
void Planner::picker(moveit_msgs::CollisionObject pickObj)
{
    if (!planning && !executing) // Scene should be locked (not updated) when planning is carried out
    {

        moveit::planning_interface::MoveGroupInterface group("panda_arm");
        moveit::planning_interface::MoveGroupInterface hand_group("hand");

        moveit::planning_interface::PlanningSceneInterface planning_scene_interface;

        if (planning_scene_interface.getAttachedObjects().count("Pen Placeholder") != 0)
        {
            group.detachObject("Pen Placeholder");
            penPlaceholder.operation = penPlaceholder.REMOVE;
            planning_scene_monitor::LockedPlanningSceneRW(psm)->processCollisionObjectMsg(penPlaceholder);
            std::vector<std::string> objects;
            objects.push_back("Pen Placeholder");
            planning_scene_interface.removeCollisionObjects(objects);
        }
        if (planning_scene_interface.getAttachedObjects().count("Eraser Placeholder") != 0)
        {
            group.detachObject("Eraser Placeholder");
            eraserPlaceholder.operation = eraserPlaceholder.REMOVE;
            planning_scene_monitor::LockedPlanningSceneRW(psm)->processCollisionObjectMsg(eraserPlaceholder);
            std::vector<std::string> objects;
            objects.push_back("Eraser Placeholder");
            planning_scene_interface.removeCollisionObjects(objects);
        }
        if (planning_scene_interface.getAttachedObjects().count(pickObj.id) != 0)
        {
            std::vector<std::string> objects;
            objects.push_back(pickObj.id);
            planning_scene_interface.removeCollisionObjects(objects);
        }


        pickPlaceFlag = false;
        objectPos[0] = pickObj.primitive_poses[0].position.x;
        objectPos[1] = pickObj.primitive_poses[0].position.y;
        objectPos[2] = pickObj.primitive_poses[0].position.z;
        objectHeight = pickObj.primitives[0].dimensions[2];
        geometry_msgs::Quaternion objectRot = pickObj.primitive_poses[0].orientation;

        objectOrientation.setValue(objectRot.x, objectRot.y, objectRot.z, objectRot.w);

        objectName = pickObj.id;

        ROS_INFO("Attempting Pick");

        executing = true;
        execution_flag = true;

        moveit::planning_interface::MoveGroupInterface::Plan my_plan;


        std::vector<double> fingerPos;
        fingerPos.resize(2);

        if (!gripperOpen) {
            fingerPos[0] = 0.033;
            fingerPos[1] = 0.033;
            hand_group.setJointValueTarget(fingerPos);
            hand_group.setPlanningTime(45.0);
            hand_group.move();
            gripperOpen = true;
        }

        toggleCollisionGripper(objectName, true, true);

        geometry_msgs::Pose target_pose = group.getCurrentPose().pose;

        target_pose.position.z -= 0.155;
        group.setPoseTarget(target_pose);
        group.setMaxVelocityScalingFactor(0.1f);
        group.setPlanningTime(45.0);
        group.move();

        if (gripperOpen) {
            ROS_INFO("Close Gripper");
            fingerPos[0] = 0.0;
            fingerPos[1] = 0.0;
            hand_group.setJointValueTarget(fingerPos);
            hand_group.setPlanningTime(45.0);
            hand_group.move();
            gripperOpen = false;
        }

        group.attachObject(objectName);

        target_pose.position.z += objectHeight + 0.05;
        group.setPoseTarget(target_pose);
        group.setMaxVelocityScalingFactor(0.1f);
        group.setPlanningTime(45.0);
        group.move();

        executing = false;
        execution_flag = false;
        toggleCollisionGripper(objectName, false, true);
    }
}

void Planner::placer(geometry_msgs::Vector3 placePosVec)
{
    if (!planning && !executing)
    {
        std_msgs::UInt32 unity_state;
        unity_state.data = 2;
        unityStatePublisher.publish(unity_state);

        ROS_INFO("Attempting Place");

        executing = true;
        execution_flag = true;

        moveit::planning_interface::MoveGroupInterface group("panda_arm");
        moveit::planning_interface::MoveGroupInterface hand_group("hand");

        moveit::planning_interface::MoveGroupInterface::Plan my_plan;

        toggleCollisionGripper(objectName, true, true);

        std::vector<double> fingerPos;
        fingerPos.resize(2);
        if (!gripperOpen) {
            fingerPos[0] = 0.035;
            fingerPos[1] = 0.035;
            hand_group.setJointValueTarget(fingerPos);
            hand_group.setPlanningTime(45.0);
            hand_group.move();
            gripperOpen = true;
        }

        group.detachObject(objectName);

        moveit::planning_interface::PlanningSceneInterface planning_scene_interface;

        if (planning_scene_interface.getAttachedObjects().count("Pen Placeholder") != 0)
        {
            group.detachObject("Pen Placeholder");
            penPlaceholder.operation = penPlaceholder.REMOVE;
            planning_scene_monitor::LockedPlanningSceneRW(psm)->processCollisionObjectMsg(penPlaceholder);
            std::vector<std::string> objects;
            objects.push_back("Pen Placeholder");
            planning_scene_interface.removeCollisionObjects(objects);
        }

        geometry_msgs::Pose target_pose = group.getCurrentPose().pose;
        target_pose.position.z += 0.1;
        group.setPoseTarget(target_pose);
        group.setMaxVelocityScalingFactor(0.1f);
        group.setPlanningTime(45.0);
        group.move();

        executing = false;
        execution_flag = false;
        toggleCollisionGripper(objectName, false, false);
    }
}

// PathPlanner using motion_planning_api
void Planner::pathPlanner(geometry_msgs::PoseArray path)
{
    // If the new request is sent from the client (meaning all the stages in the client's goalpoints array are done)
    if (stages.empty())
    {
        // Save the input path
        input_path = path;
        geometry_msgs::PoseArray stage;
        for (int i = 0; i < input_path.poses.size(); i++)
        {
            input_path.poses[i].position.z += 0.04; //-0.03 for whiteboard eraser
            moveit::planning_interface::PlanningSceneInterface planning_scene_interface;
            if (planning_scene_interface.getAttachedObjects().count("5mm lower") != 0) input_path.poses[i].position.z -= 0.005;
            if (planning_scene_interface.getAttachedObjects().count("5mm higher") != 0) input_path.poses[i].position.z += 0.005;
            if (planning_scene_interface.getAttachedObjects().count(objectName) != 0 || planning_scene_interface.getAttachedObjects().count("Toggle Lower") == 0) input_path.poses[i].position.z -= 0.07;
            input_path.poses[i].position.z -= 0.055;
            input_path.poses[i].position.x += 0.01;
            input_path.poses[i].position.y -= 0.01;

            stage.poses.push_back(input_path.poses[i]);
            if (input_path.poses[i].position.x < 0 || i == input_path.poses.size() - 1)
            {
                stages.push_back(stage);
                stage.poses.clear();
            }
        }
        ROS_INFO("Number of stages: %d", (int)stages.size());
        // // Save the input path
        // input_path = path;
        // // Divide into stages
        // int iters = input_path.poses.size() / 4; // Only maximum of 4 trajectories/goalpoints executed per stage for the experiment
        // int rem = input_path.poses.size() % 4;   // The remainder of goalpoints that the robot would execute before ending each phase
        // // First get every 4-goalpoint segments from the requested path
        // for (int i = 0; i < iters; i++)
        // {
        //     geometry_msgs::PoseArray stage;
        //     auto first = input_path.poses.cbegin() + 4 * i;
        //     auto last = input_path.poses.cbegin() + 4 * (i + 1);
        //     std::vector<geometry_msgs::Pose> vec(first, last);
        //     stage.poses = vec;
        //     stages.push_back(stage);
        // }
        // // If there are remainders, get the rest into the stages array
        // if (rem > 0)
        // {
        //     geometry_msgs::PoseArray stage;
        //     auto first = input_path.poses.cbegin() + 4 * iters;
        //     auto last = input_path.poses.cbegin() + 4 * iters + rem;
        //     std::vector<geometry_msgs::Pose> vec(first, last);
        //     stage.poses = vec;
        //     stages.push_back(stage);
        // }
    }
    // Assign the first stage to be planned
    executing_path = stages.front();
    moveit_msgs::PlanningScene scene;
    waypoint_robot_state.clear();
    robot_trajectory.trajectory.clear();
    planning = true;
    // Start planning
    // ^^^^^^^^^^^^^^
    // Save the starting Robot State
    waypoint_robot_state.push_back(robot_state::RobotState(planning_scene_monitor::LockedPlanningSceneRO(psm)->getCurrentState()));
    moveit_msgs::DisplayTrajectory planner_output_robot_trajectory;
    std::vector<moveit_msgs::RobotState> planner_output_traj_start_state_msg;
    std::vector<robot_state::RobotState> planner_output_waypoint_robot_state = waypoint_robot_state;
    for (int i = 0; i < executing_path.poses.size(); i++)
    {
        geometry_msgs::PoseStamped pose;
        pose.header.frame_id = BASE_FRAME;
        pose.pose = executing_path.poses[i];
        // Set the current robot state as a plan request's start state
        robot_state::robotStateToRobotStateMsg(planner_output_waypoint_robot_state[i], req.start_state);
        moveit_msgs::Constraints pose_goal =
            kinematic_constraints::constructGoalConstraints(EEF, pose, tolerance_pose, tolerance_angle);
        req.goal_constraints.clear();
        req.goal_constraints.push_back(pose_goal);
        planning_scene_monitor::LockedPlanningSceneRO lscene(psm);
        planning_pipeline->generatePlan(lscene, req, res);
        if (res.error_code_.val != res.error_code_.SUCCESS)
        {
            ROS_ERROR("Could not compute plan sucessfully, error code: %d", res.error_code_.val);
        }
        else
        {
            moveit_msgs::MotionPlanResponse response;
            res.getMessage(response);
            planner_output_robot_trajectory.trajectory.push_back(response.trajectory);
            // Save the start state as a message for reference
            planner_output_traj_start_state_msg.push_back(response.trajectory_start);
            // Update the robot state
            robot_state_holder = lscene->getCurrentStateUpdated(response.trajectory_start);
            robot_state_holder->setJointGroupPositions(joint_model_group, response.trajectory.joint_trajectory.points.back().positions);
            planner_output_waypoint_robot_state.push_back(*robot_state_holder);
            ROS_INFO("Plan successfully");
        }
    }
    ROS_INFO("Finish Planning");
    robot_trajectory = planner_output_robot_trajectory;
    waypoint_robot_state = planner_output_waypoint_robot_state;
    traj_start_state_msg = planner_output_traj_start_state_msg;
    planning = false;
    // Update the trajectory line
    trajectoryPoints(robot_trajectory);
    trajectory_preview.publish(robot_trajectory);
}
bool Planner::checkValidity()
{
    index.clear();
    classification.clear();
    std::vector<std::size_t> idx;
    bool need_replan = false;
    for (int i = 0; i < robot_trajectory.trajectory.size(); i++)
    {
        bool valid = planning_scene_monitor::LockedPlanningSceneRO(psm)->isPathValid(traj_start_state_msg[i], robot_trajectory.trajectory[i], PLANNING_GROUP, false, &idx);
        // Classification of invalid path
        if (!valid)
        {
            // If the trajectory the robot executing is in collision, stop the robot
            if (i == 0 && executing)
            {
                ROS_WARN("Terminate the robot");
                movegroup_ptr->stop();
                executing = false;
                temp_stop = true;
            }
            need_replan = true;
            index.push_back(idx);
            // Path is invalid at the starting state and the finishing state
            if (idx[0] == 0 && (idx[idx.size() - 1] == robot_trajectory.trajectory[i].joint_trajectory.points.size() - 1))
            {
                classification.push_back(1);
            }
            // Path is invalid at the starting state
            else if (idx[0] == 0)
            {
                classification.push_back(2);
            }
            // Path is invalid at the finishing state
            else if (idx[idx.size() - 1] == robot_trajectory.trajectory[i].joint_trajectory.points.size() - 1)
            {
                classification.push_back(3);
            }
            // None of the above (could be invalid but in the middle of the path)
            else
            {
                classification.push_back(4);
            }
        }
        // if path is valid, set 0
        else
        {
            classification.push_back(0);
            std::vector<std::size_t> rd;
            rd.push_back(0);
            index.push_back(rd);
        }
    }
    if (need_replan)
    {
        ROS_INFO("Replan required");
    }
    else if (execution_flag && !executing && robot_trajectory.trajectory.size() > 0)
    {
        ROS_INFO("Trajectory used to be invalid but not now. Replan required");
        classification.front() = 4; // Type 4
        need_replan = true;
    }
    return !need_replan;
}
void Planner::replan(const ros::TimerEvent& event)
{
    /*static int debug_count = 0;
    if (debug_count == 20)
    {
        debug_count = 0;
        ROS_INFO("Number of replan queries: %d", replan_queries_count);
    }
    else
    {
        debug_count++;
    }*/
    bool replan_failed = false;
    if (!planning) // Only runs when nothing is running to avoid conflicting with others
    {
        if (!checkValidity())
        {
            // Replan the path where there are collisions
            // Start planning
            // ^^^^^^^^^^^^^^
            planning = true;
            req.group_name = PLANNING_GROUP;
            if (temp_stop && execution_flag)
            {
                // Wait for robot's feedback
                planning = false;
                ROS_INFO("Wait for robot to stop");
                return;
            }
            // Get the order of which path to replan first
            // Ascending order (from the lowest class to the highest class)
            std::vector<int> order = vectorsort(classification);

            // std::stexecuting = 
            //     str += std::to_string(order[i]);
            //     str += ", ";
            //     str2 += std::to_string(classification[order[i]]);
            //     str2 += ", ";
            // }
            // ROS_INFO("%s", str.c_str());
            // ROS_INFO("%s", str2.c_str());

            // Placeholders for replan function
            std::vector<moveit_msgs::RobotState> replan_traj_start_state_msg(traj_start_state_msg.begin(), traj_start_state_msg.end());
            std::vector<robot_state::RobotState> replan_waypoint_robot_state(waypoint_robot_state.begin(), waypoint_robot_state.end());
            moveit_msgs::DisplayTrajectory replan_robot_trajectory = robot_trajectory;

            // Check for every invalid path
            for (int i = order.size() - 1; i >= 0; i--)
            {
                moveit_msgs::MotionPlanResponse response;
                // Four cases:
                // 1. Both first and last waypoints are in collision
                // 2. Only first waypoint is in collision
                // 3. Only last waypoint is in collision
                // 4. None of the above
                //ROS_INFO("Planning: %d", order[i]);
                // Case 1 and 3
                if (classification[order[i]] == 1 || classification[order[i]] == 3)
                {
                    // Plan to return to the old pose with valid RobotState
                    // First set the requested starting state to be the start of the trajectory
                    robot_state::robotStateToRobotStateMsg(replan_waypoint_robot_state[order[i]], req.start_state);
                    geometry_msgs::PoseStamped pose;
                    pose.header.frame_id = BASE_FRAME;
                    pose.pose = executing_path.poses[order[i]];
                    moveit_msgs::Constraints pose_goal =
                        kinematic_constraints::constructGoalConstraints(EEF, pose, tolerance_pose, tolerance_angle);
                    req.goal_constraints.clear();
                    req.goal_constraints.push_back(pose_goal);
                    planning_scene_monitor::LockedPlanningSceneRO lscene(psm);
                    planning_pipeline->generatePlan(lscene, req, res);
                    if (res.error_code_.val != res.error_code_.SUCCESS)
                    {
                        ROS_ERROR("Traj %d, case %d not sucessfully, error code: %d", order[i], classification[order[i]], res.error_code_.val);
                        planning = false;
                        return;
                    }
                    else
                    {
                        ROS_INFO("Traj %d, case %d successfully", order[i], classification[order[i]]);
                        res.getMessage(response);
                        replan_traj_start_state_msg[order[i]] = response.trajectory_start;
                        replan_robot_trajectory.trajectory[order[i]] = response.trajectory;
                        // Since new pose is now in new RobotState, also update the final state
                        replan_waypoint_robot_state[order[i] + 1].setJointGroupPositions(joint_model_group, response.trajectory.joint_trajectory.points.back().positions);
                    }
                }
                // Case 2 and 4:/collision_object_manager
                else if (classification[order[i]] == 2 || classification[order[i]] == 4)
                {
                    // Set the requested starting state of the new trajectory to be the starting state of this trajectory
                    robot_state::robotStateToRobotStateMsg(replan_waypoint_robot_state[order[i]], req.start_state);
                    // Bring back to old state
                    moveit_msgs::Constraints state_goal =
                        kinematic_constraints::constructGoalConstraints(replan_waypoint_robot_state[order[i] + 1], joint_model_group);
                    req.goal_constraints.clear();
                    req.goal_constraints.push_back(state_goal);
                    planning_scene_monitor::LockedPlanningSceneRO lscene(psm);
                    planning_pipeline->generatePlan(lscene, req, res);
                    if (res.error_code_.val != res.error_code_.SUCCESS)
                    {
                        ROS_ERROR("Traj %d, case %d not sucessfully, error code: %d", order[i], classification[order[i]], res.error_code_.val);
                        planning = false;
                        return;
                    }
                    else
                    {
                        ROS_INFO("Traj %d, case %d successfully", order[i], classification[order[i]]);
                        res.getMessage(response);
                        replan_traj_start_state_msg[order[i]] = response.trajectory_start;
                        replan_robot_trajectory.trajectory[order[i]] = response.trajectory;
                    }
                }
            }
            // Update the trajectory line
            waypoint_robot_state = replan_waypoint_robot_state;
            traj_start_state_msg = replan_traj_start_state_msg;
            robot_trajectory = replan_robot_trajectory;
            trajectoryPoints(robot_trajectory);
            trajectory_preview.publish(robot_trajectory);
            planning = false;
            if (execution_flag)
            {
                if (!executing)
                {
                    ROS_INFO("Restart the execution");
                    movegroup_ptr->asyncExecute(robot_trajectory.trajectory.front());
                    executing = true;
                }
                else
                {
                    ROS_INFO("No need to stop the trajectory");
                }
            }
            return;
        }
        else
        {
            // ROS_INFO("No need replan");
            return;
        }
    }
    else
    {
        return;
    }
}
void Planner::executionProcess(moveit_msgs::ExecuteTrajectoryActionResult result)
{
    float prev_time = ros::Time::now().toSec();
    switch (result.result.error_code.val)
    {
    case moveit_msgs::MoveItErrorCodes::PREEMPTED:
        ros::Duration(1.0f).sleep();
        ROS_WARN("Robot Terminated");
        waypoint_robot_state.front() = robot_state::RobotState(planning_scene_monitor::LockedPlanningSceneRO(psm)->getCurrentState());
        temp_stop = false;
        ROS_INFO("Waypoint Robot State updated for replanning");
        break;
    case moveit_msgs::MoveItErrorCodes::SUCCESS:
        // Wait until replan finishes to get the most updated path
        while (planning)
        {
            // If the waiting time exceeding replan frequency, must meen replan is timed out
            if (ros::Time::now().toSec() - prev_time > replan_frequency)
            {
                // Force timeout on replan function
                ROS_INFO("Replan timeout");
                planning = false;
                prev_time = ros::Time::now().toSec();
                break;
            }
            // Otherwise, just update the user every 0.2s about the waiting status
            else if (ros::Time::now().toSec() - prev_time > 0.2f)
            {
                ROS_INFO("Waiting for replan");
                prev_time = ros::Time::now().toSec();
            }
        }
        if (robot_trajectory.trajectory.size() == 1)
        {
            ROS_INFO("Executed all planned path succesfully");
            robot_trajectory.trajectory.erase(robot_trajectory.trajectory.begin());
            waypoint_robot_state.erase(waypoint_robot_state.begin());
            traj_start_state_msg.erase(traj_start_state_msg.begin());
            executing_path.poses.erase(executing_path.poses.begin());
            executing = false;
            execution_flag = false;
            // Finish the current stage
            stages.erase(stages.begin());
            if (!stages.empty())
            {
                // Back to planning for the next stages
                geometry_msgs::PoseArray dummy;
                pathPlanner(dummy);
                // And execute
                std_msgs::UInt32 command;
                command.data = 1;
                robotCommand(command);
            }
        }
        else
        {
            robot_trajectory.trajectory.erase(robot_trajectory.trajectory.begin());
            waypoint_robot_state.erase(waypoint_robot_state.begin());
            traj_start_state_msg.erase(traj_start_state_msg.begin());
            executing_path.poses.erase(executing_path.poses.begin());
            ROS_INFO("Success in the last action, try next one");
            movegroup_ptr->asyncExecute(robot_trajectory.trajectory.front());
        }
        break;
    default:
        ROS_INFO("MoveItErrorCodes: %d", result.result.error_code.val);
        break;
    }
}
// Helper function
// This helper function will return a list of trajectory indices in ascending order of categories
// (categories mentioned in replan() function)
std::vector<int> Planner::vectorsort(std::vector<int> vector)
{
    std::vector<std::pair<int, int>> a;
    std::vector<int> out;
    for (int i = 0; i < vector.size(); i++)
    {
        a.push_back(std::make_pair(vector[i], i));
    }
    std::sort(a.begin(), a.end());
    for (int i = 0; i < vector.size(); i++)
    {
        out.push_back(a[i].second);
    }
    return out;
}
// Unity State Update
void Planner::unityStateUpdate(const ros::TimerEvent& event)
{
    static const uint32_t IDLE = 1;
    static const uint32_t EXECUTING = 2;
    static const uint32_t TEMP_STOP = 3;
    static const uint32_t PREVIEW_SHOWING = 5;
    std_msgs::UInt32 unity_state;
    if ((executing && execution_flag))
    {
        unity_state.data = EXECUTING;
    }
    else if (!executing && execution_flag)
    {
        unity_state.data = TEMP_STOP;
    }
    else
    {
        unity_state.data = IDLE;
    }
    unityStatePublisher.publish(unity_state);
}
void Planner::robotCommand(std_msgs::UInt32 command)
{
    static const uint32_t EXECUTING = 1;
    static const uint32_t STOP = 2;
    static const uint32_t READY_STATE = 3;
    switch (command.data)
    {
    case EXECUTING:
        if (robot_trajectory.trajectory.size() > 0 && !execution_flag)
        {
            ROS_INFO("Execution Permission granted");
            execution_flag = true;
            movegroup_ptr->asyncExecute(robot_trajectory.trajectory.front());
            executing = true;
        }
        else
        {
            ROS_INFO("No trajectory has been added for execution");
        }
        break;
    case STOP:
        ROS_INFO("Robot termination");
        movegroup_ptr->stop();
        break;
    case READY_STATE:
        ROS_INFO("Bringing back to ready state");
        // First check if the robot is still running
        if (executing)
        {
            ROS_INFO("Robot is executing another trajectory, try later");
            return;
        }
        else
        {
            /*
            planning = true;
            planning_scene_monitor::LockedPlanningSceneRO lscene(psm);
            robot_state::RobotState ready_goal_state = robot_state::RobotState(lscene->getCurrentState());
            ready_goal_state.setToDefaultValues(joint_model_group, "ready");
            // Dummy stage variable for the executionStatus function to clear
            geometry_msgs::PoseArray dummy;
            stages.push_back(dummy);
            robot_state::robotStateToRobotStateMsg(robot_state::RobotState(lscene->getCurrentState()), req.start_state);
            moveit_msgs::Constraints state_goal =
                kinematic_constraints::constructGoalConstraints(ready_goal_state, joint_model_group);
            req.goal_constraints.clear();
            req.goal_constraints.push_back(state_goal);
            planning_pipeline->generatePlan(lscene, req, res);
            if (res.error_code_.val != res.error_code_.SUCCESS)
            {
                ROS_INFO("Plan bringing back to ready state: Unsucessful, error code: %d", res.error_code_.val);
                planning = false;
            }
            else
            {
                ROS_INFO("Plan bringing back to ready state: Sucessful");
                moveit_msgs::MotionPlanResponse response;
                res.getMessage(response);
                planning = false;
                traj_start_state_msg.push_back(response.trajectory_start);
                robot_trajectory.trajectory.push_back(response.trajectory);
                std_msgs::UInt32 command;
                command.data = EXECUTING;
                robotCommand(command);
                ROS_INFO("Bring back to ready state request sent");
            }
            */
        }
        break;
    default:
        ROS_INFO("Invalid command: %d", command.data);
        break;
    }
}
void Planner::trajectoryPoints(moveit_msgs::DisplayTrajectory robot_trajectory)
{
    ROS_INFO("Start Publishing Trajectory Line");
    geometry_msgs::PoseArray traj_points;
    for (int i = 0; i < robot_trajectory.trajectory.size(); i++)
    {
        for (int j = 0; j < robot_trajectory.trajectory[i].joint_trajectory.points.size(); j++)
        {
            forward_kinematic_solver->setJointGroupPositions(joint_model_group, robot_trajectory.trajectory[i].joint_trajectory.points[j].positions);
            const Eigen::Isometry3d& end_effector_state = forward_kinematic_solver->getGlobalLinkTransform(EEF);
            geometry_msgs::Pose traj_point;
            traj_point.position.x = end_effector_state.translation()[0];
            traj_point.position.y = end_effector_state.translation()[1];
            traj_point.position.z = end_effector_state.translation()[2];
            traj_points.poses.push_back(traj_point);
        }
    }
    trajectory_waypoints.publish(traj_points);
    ROS_INFO("Trajectory line published");
}
int main(int argc, char** argv)
{
    ros::init(argc, argv, "planner_test");
    ros::AsyncSpinner spinner(4);
    spinner.start();
    ros::NodeHandle node_handle("");
    ros::NodeHandle node_handle_private("~");

    std::vector<double> fingerPos;
    fingerPos.resize(2);
    fingerPos[0] = 0.0;
    fingerPos[1] = 0.0;
    moveit::planning_interface::MoveGroupInterface hand_group("hand");
    hand_group.setJointValueTarget(fingerPos);
    hand_group.setPlanningTime(45.0);
    hand_group.move();

    Planner myPlanner(&node_handle, &node_handle_private);
    ros::waitForShutdown();
    return 0;
}