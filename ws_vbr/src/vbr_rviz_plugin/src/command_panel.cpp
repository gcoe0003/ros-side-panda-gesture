
#include <cstdio>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPainter>
#include <QSpinBox>
#include <QTimer>
#include <QVBoxLayout>

#include "command_panel.h"

namespace vbr_rviz_plugin
{
  CommandPanel::CommandPanel(QWidget *parent) : rviz::Panel(parent)
  {
    // Create a push button
    btn_lock_path_ = new QPushButton(this);
    btn_lock_path_->setText("Lock Path");
    connect(btn_lock_path_, SIGNAL(clicked()), this, SLOT(lockPath()));

    // Create a push button
    btn_execute_ = new QPushButton(this);
    btn_execute_->setText("Execute");
    connect(btn_execute_, SIGNAL(clicked()), this, SLOT(execute()));

    // Create a push button
    btn_terminate_ = new QPushButton(this);
    btn_terminate_->setText("Terminate");
    connect(btn_terminate_, SIGNAL(clicked()), this, SLOT(terminate()));

    // Create a push button
    btn_ready_state_ = new QPushButton(this);
    btn_ready_state_->setText("Ready State");
    connect(btn_ready_state_, SIGNAL(clicked()), this, SLOT(readyState()));

    // Horizontal Layout
    auto *hlayout1 = new QHBoxLayout;
    hlayout1->addWidget(btn_lock_path_);
    hlayout1->addWidget(btn_execute_);
    hlayout1->addWidget(btn_terminate_);
    hlayout1->addWidget(btn_ready_state_);

    // Verticle layout
    auto *layout = new QVBoxLayout;
    layout->addLayout(hlayout1);
    setLayout(layout);

    btn_lock_path_->setEnabled(true);
    btn_execute_->setEnabled(true);
    btn_terminate_->setEnabled(true);
    btn_ready_state_->setEnabled(true);

    path_request = nh_.advertise<geometry_msgs::PoseArray>("/planning_path", 1);
    command = nh_.advertise<std_msgs::UInt32>("/vb_robot_command", 1);
    sub_ = nh_.subscribe("vbr_rviz_plugin_poses", 1, &CommandPanel::setPointMonitor, this);
  }
  void CommandPanel::setPointMonitor(geometry_msgs::PoseArray poses)
  {
    goal_points = poses;
  }

  void CommandPanel::lockPath()
  {
    path_request.publish(goal_points);
    ROS_INFO("Path published");
  }

  void CommandPanel::execute()
  {
    std_msgs::UInt32 cmd;
    cmd.data = 1;
    command.publish(cmd);
    ROS_INFO("Execute Command");
  }

  void CommandPanel::terminate()
  {
    std_msgs::UInt32 cmd;
    cmd.data = 2;
    command.publish(cmd);
    ROS_INFO("Terminate Command");
  }

  void CommandPanel::readyState()
  {
    std_msgs::UInt32 cmd;
    cmd.data = 3;
    command.publish(cmd);
    ROS_INFO("Ready State Command");
  }

  void CommandPanel::save(rviz::Config config) const
  {
    rviz::Panel::save(config);
  }

  void CommandPanel::load(const rviz::Config &config)
  {
    rviz::Panel::load(config);
  }
} // end namespace vbr_rviz_plugin

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(vbr_rviz_plugin::CommandPanel, rviz::Panel)
