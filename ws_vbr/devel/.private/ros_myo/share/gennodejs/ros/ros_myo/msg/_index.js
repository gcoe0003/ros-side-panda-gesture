
"use strict";

let MyoArm = require('./MyoArm.js');
let MyoPose = require('./MyoPose.js');
let EmgArray = require('./EmgArray.js');

module.exports = {
  MyoArm: MyoArm,
  MyoPose: MyoPose,
  EmgArray: EmgArray,
};
